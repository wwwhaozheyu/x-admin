pipeline {
  agent {
        kubernetes {
        cloud 'kubernetes'
        slaveConnectTimeout 30           
      yaml '''
apiVersion: v1
kind: Pod
metadata:
  labels:
    test: build
spec:
  containers:
  - name: maven
    image: registry.cn-hangzhou.aliyuncs.com/haozheyu/maven:3.8.2
    command:
    - cat
    tty: true
    imagePullPolicy: "IfNotPresent"
    volumeMounts:
      - mountPath: "/etc/localtime"
        name: "volume-2"
        readOnly: false
      - mountPath: "/root/.m2/"
        name: "volume-maven-repo"
        readOnly: false
      - mountPath: "/etc/hosts"
        name: "volume-hosts"
        readOnly: false   
    env:
      - name: "LANGUAGE"
        value: "en_US:en"
      - name: "LC_ALL"
        value: "en_US.UTF-8"
      - name: "LANG"
        value: "en_US.UTF-8"
  - name: "docker"
    image: "registry.cn-beijing.aliyuncs.com/citools/docker:19.03.9-git"
    imagePullPolicy: "IfNotPresent"
    tty: true
    volumeMounts:
      - mountPath: "/etc/localtime"
        name: "volume-2"
        readOnly: false
      - mountPath: "/var/run/docker.sock"
        name: "volume-docker"
        readOnly: false
      - mountPath: "/etc/hosts"
        name: "volume-hosts"
        readOnly: false    
        
  - name: "kubectl"
    image: "registry.cn-hangzhou.aliyuncs.com/haozheyu/kubectl:alpinev1.20.2"
    imagePullPolicy: "IfNotPresent"
    tty: true
    volumeMounts:
      - mountPath: "/etc/localtime"
        name: "volume-2"
        readOnly: false       
      - mountPath: "/root/.kube"
        name: "volume-kubeconfig"
        readOnly: false
      - mountPath: "/etc/hosts"
        name: "volume-hosts"
        readOnly: false 
 
  - name: "debug"
    image: "registry.cn-hangzhou.aliyuncs.com/haozheyu/debug-tools:16.04.3"
    imagePullPolicy: "IfNotPresent"
    tty: true
    volumeMounts:
      - mountPath: "/etc/localtime"
        name: "volume-2"
        readOnly: false       
      - mountPath: "/etc/hosts"
        name: "volume-hosts"
        readOnly: false   
      
  volumes:
    - hostPath:
        path: "/var/run/docker.sock"
      name: "volume-docker"
    - hostPath:
        path: "/usr/share/zoneinfo/Asia/Shanghai"
      name: "volume-2"
    - hostPath:
        path: "/etc/hosts"
      name: "volume-hosts"
    - name: "volume-maven-repo"
      hostPath:
        path: "/opt/m2"
    - name: "volume-kubeconfig"
      secret:
        secretName: "multi-kube-config"
'''
    }
  }

  parameters {   
    choice(name: 'buildImage', choices: ['构建镜像', '跳过构建'], description: '是否构建')
    choice(name: 'deploy', choices: ['部署','更新', '回滚'], description: '部署选项')       
  }
  environment {
    build_image =  "$params.buildImage"
    deploy =  "$params.deploy"
    git_branch =  "${BRANCH}"
    ACCESS_TOKEN = "cbf76ecde6c188c9a5134ee501b1899cb6dbf19af4fe2c8a1e3cd6f26221c0c7"
  }

  stages {

    stage('dingchat notification') {
      steps {
        container('debug') {
            sh """
            curl "https://oapi.dingtalk.com/robot/send?access_token=$ACCESS_TOKEN" \
                -H 'Content-Type: application/json' \
                -d '{"msgtype": "markdown", "markdown": {
                "title": "${buildImage}",
                "text": [
                "# ${env.JOB_NAME}\n",
                "${currentBuild.result}\n",
                "推送 ${env.BUILD_ID}次构建\n",
                "构建前->${currentBuild.duration}毫秒  \n",
                '',
                '---',
                "[build is a link](${env.BUILD_URL})"
            ]
                }}'
               """
        }     
      }
    }

    stage('run Maven') {
      steps {
        container('maven') {
          echo "${git_branch}"
          sh 'mvn clean package -Dmaven.test.skip=true'
        }
      }
    }
    stage('docker build') {
      steps {
        script {
          if ( build_image == '构建镜像' ) {
            container('docker') {
              sh """ 
              docker build -t registry.cn-hangzhou.aliyuncs.com/haozheyu/springbootadmin:${BRANCH}-${env.BUILD_ID} .
              docker login -u ${Username} -p ${Password} ${HARBOR_ADDRESS}
              docker push registry.cn-hangzhou.aliyuncs.com/haozheyu/springbootadmin:${BRANCH}-${env.BUILD_ID}
              """
            }
          } else {
            sh """ echo '跳过构建镜像' """
          }
        }
      }
    }

    stage('kubectl set image') {
      steps {
        script {
          if ( deploy == '部署' ) {
            container('kubectl') {
              sh """ 
              kubectl create deployment springbootadmin --port=8080 --image=registry.cn-hangzhou.aliyuncs.com/haozheyu/springbootadmin:${BRANCH}-${env.BUILD_ID}
              kubectl expose deployment springbootadmin --port=8080 --target-port=8080 --name=springbootadmin-service --type=NodePort
              kubectl get deployment springbootadmin
              kubectl get svc springbootadmin-service
              """
            }
          } else if(deploy == '更新') {
            container('kubectl') {
              sh """ 
                #kubectl set image deployment springbootadmin *=registry.cn-hangzhou.aliyuncs.com/haozheyu/springbootadmin:${BRANCH}-${env.BUILD_ID}
                #kubectl set image deployment sprintboot-demo sprintboot-demo=registry.cn-hangzhou.aliyuncs.com/haozheyu/sprintboot-demo:${BRANCH}-${env.BUILD_ID}
                kubectl set image deployment springbootadmin *=registry.cn-hangzhou.aliyuncs.com/haozheyu/springbootadmin:${BRANCH}-${env.BUILD_ID}
              """
            }
          } else {
            container('kubectl') {
              sh """ 
              kubectl rollout undo deployment springbootadmin
              """
            }
          }
        }
      }
    }

    stage('debug') {
      steps {
        container('debug') {
        sh """
          curl -I -m 10 -s http://springbootadmin-service.default.svc.cluster.local:8080/login
        """
        }     
      }
    }
  }
  post {
    success {
        dingtalk (
            robot: "${ACCESS_TOKEN}",
            type: 'MARKDOWN',
            text: [
                "# ${env.JOB_NAME}\n",
                "${currentBuild.result}\n",
                "第${env.BUILD_ID}次构建\n",
                "推送构建成功->${currentBuild.duration}毫秒  \n",
                '',
                '---',
                "[build is a link](${env.BUILD_URL})"
            ]
        )
    }

    failure {
         dingtalk (
            robot: "${ACCESS_TOKEN}",
            type: 'MARKDOWN',
            text: [
                "# ${env.JOB_NAME}\n",
                "${currentBuild.result}\n",
                "第${env.BUILD_ID}次构建\n",
                "推送构建失败->${currentBuild.duration}毫秒  \n",
                '',
                '---',
                "[build is a link](${env.BUILD_URL})"
            ]
        )
    }
  }
}