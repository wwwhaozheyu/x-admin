FROM registry.cn-hangzhou.aliyuncs.com/haozheyu/jdk:jdk1.8.0.202


ADD target/x-admin-1.0-SNAPSHOT.jar /x-admin-1.0-SNAPSHOT.jar
EXPOSE 8080

# 指定docker容器启动时运行jar包
ENTRYPOINT ["java", "-jar","/x-admin-1.0-SNAPSHOT.jar"]
# 指定维护者的名字
MAINTAINER 1216108213@qq.com

